module cpu(clock, reset);
  input clock, reset;

  wire [15:0] pc, pc_new, pc_valueIF, pc_valueID;
  wire [15:0] instrIF, instrID;
  reg [1:0] mux_pc_branch_select;
  reg ext_imm_sign;
  wire [5:0] instr_opcode;
  wire [3:0] instr_funct;
  reg mux_rf_rn1_select, mux_rf_rn2_select;
  reg mux_alu_in1_select, mux_alu_in2_select;
  reg in2_from_reg;
  wire [1:0] rf_rn1, rf_rn2;
  wire [15:0] rf_rd1, rf_rd2;
  wire [15:0] alu_in1, alu_in2;
  wire [15:0] alu_in1_EXE, alu_in2_EXE;
  wire [1:0] rf_wn;
  wire [15:0] alu_out, alu_out_MEM, alu_out_EXE, alu_out_WB;
  wire [3:0] alu_op_EXE;
  reg [3:0] alu_op;
  wire alu_zero;
  wire [15:0] dm_od;
  reg dm_w;
  reg rf_w, mux_rf_wd_select;
  wire [15:0] dm_id;
  wire [15:0] rf_wd, rf_wd_WB;
  wire [5:0] instr_imm;
  wire [9:0] instr_addr;
  wire rf_w_EXE;
  wire [1:0] rf_wn_EXE;
  wire dm_w_EXE;
  wire [15:0] dm_id_EXE;
  wire rf_w_MEM;
  wire [1:0] rf_wn_MEM;
  wire dm_w_MEM;
  wire [15:0] dm_id_MEM;
  wire rf_w_WB;
  wire [1:0] rf_wn_WB;
  wire [5:0] instr_imm_EXE;
  wire [1:0] rf_rn1_EXE, rf_rn2_EXE;

  wire in1_select_EXE, in2_select_EXE;
  wire ext_imm_sign_EXE;

  // HAZARD

  wire hazard;

  wire [1:0] alu_rf_rn1; // RN1 will be read in EXE
  assign alu_rf_rn1 = mux_alu_in1_select == 0 ? rf_rn1 : rf_rn2;

  assign hazard =
    (rf_w_EXE == 1 && (rf_wn_EXE == alu_rf_rn1 || (in2_from_reg && rf_wn_EXE == rf_rn2))) ||
    (rf_w_MEM == 1 && (rf_wn_MEM == alu_rf_rn1 || (in2_from_reg && rf_wn_MEM == rf_rn2)));
    // add dm

  // PC

  register_sload_sreset #(.W(16), .DV(0)) _program_counter(
    .i(pc_new),
    .l(~hazard),
    .clk(clock),
    .rst(reset),
    .o(pc)
  );

  wire [15:0] ext_imm =
  ext_imm_sign
    ? {{10{instr_imm[5]}}, instr_imm}
    : {10'd0, instr_imm};
  // == расширитель константы addr инструкции ==
  wire [15:0] ext_addr = {6'd0, instr_addr};
  // == +1 к значению счётчика команд ==
  wire [15:0] pc_next_value = pc + 1;
  // == сложение счётчика команд с константой при условном ветвлении
  wire [15:0] pc_branch_value = pc_next_value + ext_imm;

  mux #(.DW(16), .CW(2)) _mux_pc_branch(
    .i({pc_next_value, pc_branch_value, ext_addr, 16'd0}),
    .s(mux_pc_branch_select),
    .o(pc_new)
  );

  // STAGES

  stageIF stageIF(
    // in
    .clock(clock),
    .reset(reset),
    .pc(pc),
    // out
    .instr(instrIF)
  );

  regIF2ID regIF2ID(
    // in
    .clock(clock),
    .reset(reset),
    .instr_in(instrIF),
    .hazard(hazard),
    // out
    .instr_out(instrID)
  );

  always @(*)
  begin
    mux_pc_branch_select = 0;
    if(instr_opcode == `OPCODE_BEQ && alu_zero == 1) mux_pc_branch_select = 1;
    if(instr_opcode == `OPCODE_BNE && alu_zero == 0) mux_pc_branch_select = 1;
    if(instr_opcode == `OPCODE_J) mux_pc_branch_select = 2;
  end

  always @(*)
  begin
    mux_rf_rn1_select = 1;
    mux_rf_rn2_select = 1;
    if(instr_opcode == `OPCODE_BEQ ||
       instr_opcode == `OPCODE_BNE ||
       instr_opcode == `OPCODE_LW ||
       instr_opcode == `OPCODE_SW)
    begin
      mux_rf_rn1_select = 0;
      mux_rf_rn2_select = 0;
    end
  end

  always @(*)
  begin
    mux_alu_in1_select = 0;
    if(instr_opcode == `OPCODE_LW ||
       instr_opcode == `OPCODE_SW) mux_alu_in1_select = 1;
  end

  always @(*)
  begin
    mux_alu_in2_select = 0;
    in2_from_reg = 1;
    if(instr_opcode == `OPCODE_ADDIU ||
       instr_opcode == `OPCODE_ADDI ||
       instr_opcode == `OPCODE_ANDIU ||
       instr_opcode == `OPCODE_ANDI ||
       instr_opcode == `OPCODE_ORIU ||
       instr_opcode == `OPCODE_ORI ||
       instr_opcode == `OPCODE_SLTIU ||
       instr_opcode == `OPCODE_SLTI ||
       instr_opcode == `OPCODE_LW ||
       instr_opcode == `OPCODE_SW) begin
         mux_alu_in2_select = 1;
         in2_from_reg = 0;
      end
  end

  always @(*)
  begin
    ext_imm_sign = 1;
    if(instr_opcode == `OPCODE_ADDIU ||
       instr_opcode == `OPCODE_ANDIU ||
       instr_opcode == `OPCODE_ORIU ||
       instr_opcode == `OPCODE_SLTIU) ext_imm_sign = 0;
  end

  always @(*)
  begin
    rf_w = 0;
    if(instr_opcode == `OPCODE_AR ||
       instr_opcode == `OPCODE_ADDIU ||
       instr_opcode == `OPCODE_ADDI ||
       instr_opcode == `OPCODE_ANDIU ||
       instr_opcode == `OPCODE_ANDI ||
       instr_opcode == `OPCODE_ORIU ||
       instr_opcode == `OPCODE_ORI ||
       instr_opcode == `OPCODE_SLTIU ||
       instr_opcode == `OPCODE_SLTI ||
       instr_opcode == `OPCODE_LW
      ) if (~hazard) rf_w = 1;
  end

  stageID stageID(
    // in
    .clock(clock),
    .reset(reset),
    .instr(instrID),
    .mux_rf_rn1_select(mux_rf_rn1_select),
    .mux_rf_rn2_select(mux_rf_rn2_select),
    // out
    .instr_opcode(instr_opcode),
    .instr_funct(instr_funct),
    .rf_rn1(rf_rn1),
    .rf_rn2(rf_rn2),
    .rf_wn(rf_wn),
    .instr_imm(instr_imm),
    .instr_addr(instr_addr)
  );

  always @(*)
  begin
    alu_op = 0;
        if(instr_opcode == `OPCODE_AR) alu_op = instr_funct;
        if(instr_opcode == `OPCODE_ADDIU ||
           instr_opcode == `OPCODE_ADDI ||
           instr_opcode == `OPCODE_LW ||
           instr_opcode == `OPCODE_SW) alu_op = `FUNCT_ADD;
        if(instr_opcode == `OPCODE_ANDIU ||
           instr_opcode == `OPCODE_ANDI) alu_op = `FUNCT_AND;
        if(instr_opcode == `OPCODE_ORIU ||
           instr_opcode == `OPCODE_ORI) alu_op = `FUNCT_OR;
        if(instr_opcode == `OPCODE_SLTIU) alu_op = `FUNCT_SLTU;
        if(instr_opcode == `OPCODE_SLTI) alu_op = `FUNCT_SLT;
        if(instr_opcode == `OPCODE_BEQ ||
           instr_opcode == `OPCODE_BNE) alu_op = `FUNCT_SUB;
  end

  wire [3:0] alu_op_HAZ;
  assign alu_op_HAZ = hazard ? 0 : alu_op;

  regID2EXE regID2EXE(
    // in
    .clock(clock),
    .reset(reset),
    .rf_rn1_in(rf_rn1),
    .rf_rn2_in(rf_rn2),
    .instr_imm_in(instr_imm),
    .in1_select_in(mux_rf_rn1_select),
    .in2_select_in(mux_rf_rn2_select),
    .alu_op_in(alu_op_HAZ),
    .rf_w_in(rf_w),
    .rf_wn_in(rf_wn),
    .dm_w_in(dm_w),
    .dm_id_in(rf_rd1),
    .ext_imm_sign_in(ext_imm_sign),
    // out
    .rf_rn1_out(rf_rn1_EXE),
    .rf_rn2_out(rf_rn2_EXE),
    .instr_imm_out(instr_imm_EXE),
    .in1_select_out(in1_select_EXE),
    .in2_select_out(in2_select_EXE),
    .alu_op_out(alu_op_EXE),
    .rf_w_out(rf_w_EXE),
    .rf_wn_out(rf_wn_EXE),
    .dm_w_out(dm_w_EXE),
    .dm_id_out(dm_id_EXE),
    .ext_imm_sign_out(ext_imm_sign_EXE)
  );

  stageEXE stageEXE(
    //in
    .clock(clock),
    .reset(reset),
    .rf_rd1(rf_rd1),
    .rf_rd2(rf_rd2),
    .in1_select(in1_select_EXE),
    .in2_select(in2_select_EXE),
    .instr_imm(instr_imm_EXE),
    .ext_imm_sign(ext_imm_sign_EXE),
    .alu_op(alu_op_EXE),
    //out
    .alu_out(alu_out),
    .alu_zero(alu_zero)
  );

  regEXE2MEM regEXE2MEM(
    // in
    .clock(clock),
    .reset(reset),
    .alu_out_in(alu_out),
    .rf_w_in(rf_w_EXE),
    .rf_wn_in(rf_wn_EXE),
    .dm_w_in(dm_w_EXE),
    .dm_id_in(dm_id_EXE),
    // out
    .alu_out_out(alu_out_MEM),
    .rf_w_out(rf_w_MEM),
    .rf_wn_out(rf_wn_MEM),
    .dm_w_out(dm_w_MEM),
    .dm_id_out(dm_id_MEM)
  );

  always @(*)
  begin
    dm_w = 0;
    if(~hazard && instr_opcode == `OPCODE_SW) dm_w = 1;
  end

  stageMEM stageMEM(
    // in
    .clock(clock),
    .reset(reset),
    .id(dm_id_MEM),
    .addr(alu_out_MEM),
    .w(dm_w_MEM),
    // out
    .od(dm_od)
  );

  always @(*)
  begin
    mux_rf_wd_select = 0;
    if(instr_opcode == `OPCODE_LW) mux_rf_wd_select = 1;
  end

  regMEM2WB regMEM2WB(
    // in
    .clock(clock),
    .reset(reset),
    .alu_out_in(alu_out_MEM),
    .rf_w_in(rf_w_MEM),
    .rf_wn_in(rf_wn_MEM),
    // out
    .alu_out_out(alu_out_WB),
    .rf_w_out(rf_w_WB),
    .rf_wn_out(rf_wn_WB)
  );

  stageWB stageWB(
    // in
    .clock(clock),
    .reset(reset),
    .dm_od(dm_od),
    .alu_out(alu_out_WB),
    .mux_rf_wd_select(mux_rf_wd_select),
    // out
    .wb_out(rf_wd)
  );

  register_file _register_file(
    // in
    .rn1(rf_rn1_EXE),
    .rn2(rf_rn2_EXE),
    .wn(rf_wn_WB),
    .w(rf_w_WB),
    .rst(reset),
    .clk(clock),
    .wd(rf_wd),
    // out
    .rd1(rf_rd1),
    .rd2(rf_rd2)
  );

endmodule
