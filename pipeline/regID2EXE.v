module regID2EXE(clock, reset, hazard, ext_imm_sign_in, ext_imm_sign_out, in1_select_in, in1_select_out, in2_select_in, in2_select_out, rf_rn1_in, rf_rn2_in, rf_rn1_out, rf_rn2_out, alu_op_in, alu_op_out, rf_w_in, rf_w_out, rf_wn_in, rf_wn_out, dm_w_in, dm_w_out, dm_id_in, dm_id_out, instr_imm_in, instr_imm_out);
  input clock, reset;
  input [3:0] alu_op_in;
  input rf_w_in;
  input [1:0] rf_rn1_in, rf_rn2_in;
  input [1:0] rf_wn_in;
  input dm_w_in;
  input [15:0] dm_id_in;
  input hazard;
  input [5:0] instr_imm_in;
  input in1_select_in, in2_select_in;
  input ext_imm_sign_in;

  output reg [3:0] alu_op_out;
  output reg rf_w_out;
  output reg [1:0] rf_rn1_out, rf_rn2_out;
  output reg [1:0] rf_wn_out;
  output reg dm_w_out;
  output reg [15:0] dm_id_out;
  output reg [5:0] instr_imm_out;
  output reg in1_select_out, in2_select_out;
  output reg ext_imm_sign_out;

  always @ ( posedge clock ) begin
    if (reset) begin
      rf_rn1_out <= 0;
      rf_rn2_out <= 0;
      alu_op_out <= 0;
      rf_w_out <= 0;
      rf_wn_out <= 0;
      dm_w_out <= 0;
      dm_id_out <= 0;
      instr_imm_out <= 0;
      in1_select_out <= 0;
      in2_select_out <= 0;
      ext_imm_sign_out <= 0;
    end else begin
      rf_rn1_out <= rf_rn1_in;
      rf_rn2_out <= rf_rn2_out;
      alu_op_out <= alu_op_in;
      rf_w_out <= rf_w_in;
      rf_wn_out <= rf_wn_in;
      dm_w_out <= dm_w_in;
      dm_id_out <= dm_id_in;
      instr_imm_out <= instr_imm_in;
      in1_select_out <= in1_select_in;
      in2_select_out <= in2_select_in;
      ext_imm_sign_out <= ext_imm_sign_in;
    end
    if (hazard) begin
      alu_op_out <= 0;
    end
  end

 endmodule
