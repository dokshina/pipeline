module regIF2ID(clock, reset, instr_in, hazard, instr_out);
  input clock, reset;
  input [15:0] instr_in;
  input hazard;

  output reg [15:0] instr_out;

  always @ ( posedge clock ) begin
    if (reset) begin
      instr_out <= 0;
    end else begin
      if (~hazard) begin
        instr_out <= instr_in;
      end
    end
  end

endmodule
