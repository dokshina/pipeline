module stageIF(clock, reset, pc, instr);
  input clock, reset;
  input [15:0] pc;
  output [15:0] instr;

  instruction_memory _instruction_memory(
    .addr(pc),
    .instr(instr)
  );

endmodule
