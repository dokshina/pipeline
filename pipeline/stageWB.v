module stageWB(clock, reset, mux_rf_wd_select, dm_od, alu_out, wb_out);
  input clock, reset;
  input [15:0] dm_od;
  input [15:0] alu_out;
  input mux_rf_wd_select;

  output[15:0] wb_out;

  assign wb_out = (mux_rf_wd_select) ? dm_od : alu_out;

endmodule
