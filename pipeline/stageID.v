module stageID(clock, reset, instr, mux_rf_rn1_select, mux_rf_rn2_select, instr_opcode, instr_funct, rf_rn1, rf_rn2, rf_wn, instr_imm, instr_addr);
  input clock, reset;
  input [15:0] instr;
  input mux_rf_rn1_select, mux_rf_rn2_select;

  output [5:0] instr_opcode;
  output [3:0] instr_funct;
  output [1:0] rf_rn1, rf_rn2;
  output [1:0] rf_wn;
  output [5:0] instr_imm;
  output [9:0] instr_addr;

  assign instr_opcode = instr[15:10];
  assign instr_funct = instr[3:0];

  wire [1:0] instr_n1 = instr[9:8];
  wire [1:0] instr_n2 = instr[7:6];
  wire [1:0] instr_n3 = instr[5:4];

  assign instr_imm = instr[5:0];
  assign instr_addr = instr[9:0];

  assign rf_wn = instr_n1;

  // == мультиплексор: первый номер считываемого регистра ==
  mux #(.DW(2), .CW(1)) _mux_rf_rn1(
    .i({instr_n1, instr_n2}),
    .s(mux_rf_rn1_select),
    .o(rf_rn1)
  );

  // == мультиплексор: второй номер считываемого регистра ==
  mux #(.DW(2), .CW(1)) _mux_rf_rn2(
    .i({instr_n2, instr_n3}),
    .s(mux_rf_rn2_select),
    .o(rf_rn2)
  );

endmodule
