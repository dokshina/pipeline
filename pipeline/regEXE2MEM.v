module regEXE2MEM(clock, reset, alu_out_in, rf_w_in, alu_out_out, rf_w_out, rf_wn_in, rf_wn_out, dm_w_in, dm_w_out, dm_id_in, dm_id_out);
  input clock, reset;
  input [15:0] alu_out_in;
  input rf_w_in;
  input [1:0] rf_wn_in;
  input dm_w_in;
  input [15:0] dm_id_in;

  output reg [15:0] alu_out_out;
  output reg rf_w_out;
  output reg [1:0] rf_wn_out;
  output reg dm_w_out;
  output reg [15:0] dm_id_out;

  always @ ( posedge clock ) begin
    if (reset) begin
      alu_out_out <= 0;
      rf_w_out <= 0;
      rf_wn_out <= 0;
      dm_w_out <= 0;
      dm_id_out <= 0;
    end else begin
      alu_out_out <= alu_out_in;
      rf_w_out <= rf_w_in;
      rf_wn_out <= rf_wn_in;
      dm_w_out <= dm_w_in;
      dm_id_out <= dm_id_in;
    end
  end

endmodule
