module stageMEM(clock, reset, id, addr, w, od);
  input clock, reset;
  input [15:0] id;
  input [15:0] addr;
  input w;

  output [15:0] od;

  data_memory _data_memory(
    // in
    .addr(addr),
    .id(id),
    .clk(clock),
    .w(w),
    // out
    .od(od)
  );
endmodule
