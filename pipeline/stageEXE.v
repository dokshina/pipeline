module stageEXE(clock, reset, rf_rd1, rf_rd2, instr_imm, in1_select, in2_select,, alu_op, alu_out, alu_zero, ext_imm_sign);
  input clock, reset;
  input [5:0] instr_imm;
  input [3:0] alu_op;
  input in1_select, in2_select;
  input [15:0] rf_rd1, rf_rd2;
  input ext_imm_sign;

  output [15:0] alu_out;
  wire [15:0] alu_in1, alu_in2;
  output alu_zero;

  // == расширитель константы imm инструкции ==
  wire [15:0] ext_imm =
    ext_imm_sign
    ? {{10{instr_imm[5]}}, instr_imm}
    : {10'd0, instr_imm};
  // == мультиплексор: первый вход алу ==
  mux #(.DW(16), .CW(1)) _mux_alu_in1(
    .i({rf_rd1, rf_rd2}),
    .s(in1_select),
    .o(alu_in1)
  );

  // == мультиплексор: второй вход алу ==
  wire mux_alu_in2_select;
  mux #(.DW(16), .CW(1)) _mux_alu_in2(
    .i({rf_rd2, ext_imm}),
    .s(in2_select),
    .o(alu_in2)
  );

  alu _alu(
    // in
    .in1(alu_in1),
    .in2(alu_in2),
    .op(alu_op),
    // out
    .out(alu_out),
    .zero(alu_zero)
  );
endmodule
